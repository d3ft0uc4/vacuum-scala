import sbt.Keys._
import sbt._


object Resolvers {
  val typeSafeRepo = Classpaths.typesafeReleases

  def commonResolvers = Seq(typeSafeRepo)
}

object Dependencies {
  private val log4jVersion = "2.0.2"
  val jest = "io.searchbox" % "jest" % "2.0.3"
  val log4jApi = "org.apache.logging.log4j" % "log4j-api" % log4jVersion
  val log4jCore = "org.apache.logging.log4j" % "log4j-core" % log4jVersion
  val quartz = "org.quartz-scheduler" % "quartz" % "2.2.1"
  val cfg = "com.typesafe" % "config" % "1.3.0"
  val slf4jApi = "org.slf4j" % "slf4j-api" % "1.7.22"
  val slf4j = "org.slf4j" % "slf4j-simple" % "1.7.22"
}


object BuildSettings {

  val Organization = "com.ytrail.datawerks"
  val Name = "vacuum"
  val Version = "0.0.1-SNAPSHOT"
  val ScalaVersion = "2.11.8"
  val IsSnapshot = Version.trim.endsWith("SNAPSHOT")

  val buildSettings = Seq(
    organization := Organization,
    name := Name,
    version := Version,
    scalaVersion := ScalaVersion
  )
}

object MainBuild extends Build {

  import BuildSettings._
  import Dependencies._
  import Resolvers._

  val deps = Seq(jest, log4jApi, log4jCore, quartz, cfg, slf4jApi, slf4j)

  javacOptions ++= Seq("-encoding", "UTF-8")

  lazy val main = Project(
    Name,
    file("."),
    settings = buildSettings ++ Defaults.coreDefaultSettings ++
      Seq(
        libraryDependencies ++= deps,
        resolvers := commonResolvers
      ))


}

