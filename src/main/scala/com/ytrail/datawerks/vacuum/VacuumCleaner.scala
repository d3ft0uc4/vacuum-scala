package com.ytrail.datawerks.vacuum

import java.io.Reader

import io.searchbox.client.{JestClientFactory, JestClient}
import io.searchbox.client.config.HttpClientConfig
import io.searchbox.core.{Delete, Bulk}
import com.ytrail.datawerks.vacuum.util.Logging
import scala.collection.JavaConverters._
import com.typesafe.config.{ConfigFactory, Config}

/**
 * TODO Add element description, main objective and relations
 *
 * @author Sasa Djuric
 */
abstract class VacuumCleaner(val cfg: VacuumConfig) extends Logging {

  def name: String = cfg.name

  def es: EsInstance = cfg.es

  def documents(): ScrollIterable

  def run(): Int = {
    var cleaned = 0
    val docs = documents().iterator()
    val client = es.connect()
    while (docs.hasNext) {
      val del = docs.take(cfg.bulkSize)
      val bb = new Bulk.Builder()
      del.foreach { case x =>
        val builder = new Delete.Builder(x).index(cfg.index).`type`(cfg.esType.orNull)
        bb.addAction(builder.build())
      }
      bb.defaultIndex(cfg.index).defaultType(cfg.esType.orNull)
      val res = client.execute(bb.build())
      if (res.isSucceeded)
        cleaned += del.size
      else {
        log.warn("bulk action did not succeed ${res.errorMessage}: items failed: ${res.failedItems}")

        cleaned += res.getItems.asScala.count {
          case x => x.error == null
        }
      }
    }
    cleaned
  }
}

case class VacuumConfig(name: String, clazz: String, index: String, esType: Option[String], es: EsInstance, bulkSize: Int, fetchSize: Int, cron: String, cfg: Config)


object VacuumConfig {

  def read(source: Reader): VacuumConfig = {
    val cfg = ConfigFactory.parseReader(source)
    val clazz = cfg.getString("class")
    val name = cfg.getString("name")
    val index = cfg.getString("index")
    val esType = cfg.getString("esType") match {
      case null => None
      case x => Some(x)
    }
    val bulkSize = if (cfg.hasPath("bulkSize")) cfg.getInt("bulkSize") else 100
    val fetchSize = if (cfg.hasPath("fetchSize")) cfg.getInt("fetchSize") else 1000
    val cron = cfg.getString("cron")
    val es = {
      val self = cfg.getConfig("es")
      val host = self.getString("host")
      val port = self.getInt("port")
      val auth = if (self.hasPath("auth")) {
        {
          val cred = self.getConfig("auth")
          Some(EsAuthorization(cred.getString("user"), cred.getString("pwd")))
        }
      } else None
      EsInstance(host, port, auth)
    }
    VacuumConfig(name, clazz, index, esType, es, bulkSize, fetchSize, cron, if (cfg.hasPath("cfg")) cfg.getConfig("cfg") else ConfigFactory.empty())
  }

  def create(cfg: VacuumConfig): VacuumCleaner = {
    val clazz = Class.forName(cfg.clazz)
    val ctor = clazz.getConstructor(classOf[VacuumConfig])
    ctor.newInstance(cfg).asInstanceOf[VacuumCleaner]
  }
}

case class EsAuthorization(user: String, pwd: String) {
  def amend(builder: HttpClientConfig.Builder): HttpClientConfig.Builder = builder.defaultCredentials(user, pwd)
}

case class EsInstance(host: String, port: Int, auth: Option[EsAuthorization]) {
  def connect(): JestClient = new JestClientFactory() {
    val builder: HttpClientConfig.Builder = {
      val bld = new HttpClientConfig.Builder(s"http://$host:$port")
      auth match {
        case Some(cred) => cred.amend(bld)
        case None => bld
      }
    }

    setHttpClientConfig(builder.build())
  }.getObject
}