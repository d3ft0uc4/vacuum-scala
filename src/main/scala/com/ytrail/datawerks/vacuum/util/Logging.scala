package com.ytrail.datawerks.vacuum.util

import org.slf4j.{Logger, LoggerFactory}

/**
 * simple logging utility
 *
 * @author Sasa Djuric
 */
trait Logging {

  def log: Logger = LoggerFactory.getLogger(this.getClass)

}
