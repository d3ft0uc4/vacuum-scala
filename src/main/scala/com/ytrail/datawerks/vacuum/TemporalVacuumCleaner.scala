package com.ytrail.datawerks.vacuum

import java.time.format.DateTimeFormatter
import java.time.{Duration, LocalDate, LocalDateTime, Period}
import com.typesafe.config.Config

/**
 * TODO Add element description, main objective and relations
 *
 * @author Sasa Djuric
 */

trait TimeInitializer {
  def init: LocalDateTime

  def adjusts(ldt: LocalDateTime): LocalDateTime

  def format(adj: Option[LocalDateTime] = None): String = adj match {
    case Some(a) => adjusts(a).format(TimeInitializer.fmt)
    case None => init.format(TimeInitializer.fmt)
  }


}

object TimeInitializer {
  val fmt = DateTimeFormatter.ISO_LOCAL_DATE_TIME

  def read(cfg: Config): TimeInitializer = {

    if (cfg.hasPath("period")) {
      new PeriodInitializer(Period.parse(cfg.getString("period")))
    } else if (cfg.hasPath("duration")) {
      try {
        new DurationInitializer(Duration.parse(cfg.getString("duration")))
      } catch {
        case e: Exception => throw new IllegalArgumentException(s"problem parsing ${cfg.getString("duration")}", e)
      }
    } else if (cfg.hasPath("date")) {
      new DateInitializer(DateInitializer.read(cfg.getString("date")))
    } else if (cfg.hasPath("datetime")) {
      new DateTimeInitializer(DateTimeInitializer.read(cfg.getString("date")))
    } else throw new IllegalArgumentException(s"bad config $cfg")
  }
}

class PeriodInitializer(val period: Period) extends TimeInitializer {
  override def init: LocalDateTime = LocalDateTime.now().plus(period)

  override def adjusts(ldt: LocalDateTime) = ldt.plus(period)
}

class DurationInitializer(val duration: Duration) extends TimeInitializer {
  override def init: LocalDateTime = LocalDateTime.now().plus(duration)

  override def adjusts(ldt: LocalDateTime) = ldt.plus(duration)
}

class DateInitializer(date: LocalDate) extends TimeInitializer {
  override val init: LocalDateTime = date.atStartOfDay()

  override def adjusts(ldt: LocalDateTime): LocalDateTime = init

}

object DateInitializer {

  private val fmts = Seq(DateTimeFormatter.BASIC_ISO_DATE, DateTimeFormatter.ISO_DATE, DateTimeFormatter.ISO_LOCAL_DATE)

  def read(s: String): LocalDate = {
    var e: Option[Exception] = null
    fmts.foreach {
      case fmt =>
    }
    for (f <- fmts) {
      try {
        return LocalDate.parse(s, f)
      } catch {
        case _ => throw new Exception()
        case ex: Exception =>
          e = Some(ex)
      }
    }
    e match {
      case Some(ex) => throw new IllegalArgumentException(s"cant parse $s. last exception $ex")
      case None =>
    }
    throw new IllegalArgumentException(s"cant parse $s.")
  }

}


class DateTimeInitializer(dateTime: LocalDateTime) extends TimeInitializer {
  override val init: LocalDateTime = dateTime

  override def adjusts(ldt: LocalDateTime): LocalDateTime = init

}

object DateTimeInitializer {
  private val fmts = Seq(DateTimeFormatter.ISO_DATE_TIME, DateTimeFormatter.ISO_LOCAL_DATE_TIME, DateTimeFormatter.ISO_OFFSET_DATE_TIME)

  def read(s: String): LocalDateTime = {
    var e: Option[Exception] = null
    fmts.foreach {
      case fmt =>
    }
    for (f <- fmts) {
      try {
        return LocalDateTime.parse(s, f)
      } catch {
        case _ => throw new Exception()
        case ex: Exception =>
          e = Some(ex)
      }
    }
    e match {
      case Some(ex) => throw new IllegalArgumentException(s"cant parse $s. last exception $ex")
      case None =>
    }
    throw new IllegalArgumentException(s"cant parse $s.")
  }
}

class TemporalVacuumCleaner(cfg: VacuumConfig) extends VacuumCleaner(cfg) {
  val field = cfg.cfg.getString("field")
  val init = TimeInitializer.read(cfg.cfg.getConfig("init"))

  override def documents(): ScrollIterable = {
    val q = """
        {
          "query" : {
            "bool" : {
              "filter" : {
                "range" : {
                  "$field" : {
                    "lt" : "${init.format()}"
                  }
                }
              }
            }
          }
        }
            """
    new ScrollIterable(cfg.index, cfg.esType, q, es, cfg.fetchSize)
  }
}

class TermsVacuumCleaner(cfg: VacuumConfig) extends VacuumCleaner(cfg) {
  val field = cfg.cfg.getString("field")
  val terms = cfg.cfg.getStringList("terms")

  override def documents(): ScrollIterable = {
    val q = """
        {
          "query" : {
            "bool" : {
              "filter" : {
                "terms" : {
                  "$field" : [${terms.map { "\"$it\"" }.joinToString(",")}]
                }
              }
            }
          }
        }
            """
    new ScrollIterable(cfg.index, cfg.esType, q, es, cfg.fetchSize)
  }
}