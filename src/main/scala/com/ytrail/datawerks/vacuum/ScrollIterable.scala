package com.ytrail.datawerks.vacuum

import com.google.gson.JsonArray
import io.searchbox.core.{SearchScroll, ClearScroll, Search}
import io.searchbox.params.Parameters

/**
 * TODO Add element description, main objective and relations
 *
 * @author Sasa Djuric
 */
class ScrollIterable(val index: String, val esType: Option[String], val query: String, val esInstance: EsInstance, val bulk: Int) extends Iterable[String] {
  override def iterator(): Iterator[String] = new Iterator[String] {
    val client = esInstance.connect()
    var current: JsonArray = null
    var curIdx = -1
    var scrollId: String = null
    var closed = false
    val s: Search.Builder = new Search.Builder(query).addIndex(index)

    esType.map(t => s.addType(t))
    val q = s.setParameter(Parameters.SIZE, bulk).setParameter(Parameters.SCROLL, "1m").build()
    val res = client.execute(q)
    scrollId = res.getJsonObject.getAsJsonObject("_scroll_id").getAsString
    current = res.getJsonObject.getAsJsonObject("hits").getAsJsonObject("hits").getAsJsonArray
    println(res.getJsonObject.getAsJsonObject("hits").getAsJsonObject("total"))

    override def hasNext(): Boolean =
      if (current.size() == 0) {
        if (!closed) {
          val cls: ClearScroll = new ClearScroll.Builder().addScrollId(scrollId).build()
          client.execute(cls)
          client.shutdownClient()
          closed = true
        }
        false
      } else if (curIdx + 1 < current.size()) {
        true
      } else {
        val scroll = new SearchScroll.Builder(scrollId, "1m").build()
        val res = client.execute(scroll)
        curIdx = -1
        current = res.getJsonObject.getAsJsonObject("hits").getAsJsonObject("hits").getAsJsonArray
        hasNext()
      }

    override def next() = {
      curIdx = curIdx + 1
      current.get(curIdx).getAsJsonObject.get("_id").toString
    }


  }


}