package com.ytrail.datawerks.vacuum

import org.quartz._
import com.ytrail.datawerks.vacuum.util.Logging
import scala.collection.JavaConverters._

/**
 * TODO Add element description, main objective and relations
 *
 * @author Sasa Djuric
 */
class SchedulingService(val scheduler: Scheduler, cfgs: Iterable[VacuumConfig]) extends Logging {

  if (!scheduler.isStarted) scheduler.start()
  cfgs.foreach(addVacuumCleaner)


  def addVacuumCleaner(vacuum: VacuumConfig) = {

    val job = JobBuilder
      .newJob(classOf[VacuumJob])
      .withIdentity(vacuum.name, VacuumJob.GROUP)
      .usingJobData(new JobDataMap(Map(VacuumJob.DATA_KEY -> VacuumConfig.create(vacuum)).asJava))
      .build()

    val trigger = TriggerBuilder
      .newTrigger()
      .withSchedule(CronScheduleBuilder.cronSchedule(vacuum.cron))
      .forJob(job)
      .build()

    scheduler.scheduleJob(job, trigger)
  }
}

class VacuumJob extends Job with Logging {

  override def execute(context: JobExecutionContext) {
    log.info(s"starting com.ytrail.datawerks.vacuum ${context.getJobDetail.getKey}")
    val vac = context.getMergedJobDataMap.get(VacuumJob.DATA_KEY).asInstanceOf[VacuumCleaner]
    log.info(s"cleaner ${vac.name}")
    val res = vac.run()
    log.info(s"${vac.name} cleaned $res")
  }

}

object VacuumJob {
  val GROUP = "com.ytrail.datawerks.vacuum"
  val DATA_KEY = "com.ytrail.datawerks.vacuum"
}
