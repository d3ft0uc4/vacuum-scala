package com.ytrail.datawerks.vacuum

import java.io.FileReader
import java.nio.file.{Files, Paths}
import org.quartz.impl.StdSchedulerFactory
import scala.collection.JavaConverters._

object Main {

  def main(args: Array[String]) {
    val scheduler = new StdSchedulerFactory().getScheduler

    val cfgs = Files.list(Paths.get("./conf")).iterator().asScala.filter {
      _.getFileName.toString.endsWith(".conf")
    }.map {
      x => new FileReader(x.toFile)
    }.map {
      VacuumConfig.read(_)
    }.toSeq
    new SchedulingService(scheduler, cfgs.filter(_.isInstanceOf[VacuumConfig]))
  }
}

